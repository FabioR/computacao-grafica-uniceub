// CasaComRotacao.cpp 
// Um programa OpenGL simples que abre uma janela GLUT, 
// altera a escala e desenha uma casa.
//

#include <stdlib.h>
#include <GL/glut.h>
#include <math.h>

// Fun��o callback de redesenho da janela de visualiza��o
void Desenha(void)
{    
	
	glClear(GL_COLOR_BUFFER_BIT);
}

void bandeira(){
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glScalef(2,2,1);
	
	glColor3f(0,0.5,0);
	
	glBegin(GL_QUADS);
		glVertex2f(-40,-20);
        glVertex2f(40,-20);
        glVertex2f(40,20);
        glVertex2f(-40,20);
        
        glColor3f(1,1,0);
        
        glVertex2f(0,-15);
        glVertex2f(35,0);
        glVertex2f(0,15);
        glVertex2f(-35,0);
        
    glEnd();
    
    float theta, raio;
    raio=10;
    
    glColor3f(0,0,1);
    
    glBegin(GL_POLYGON);
    	for(int i =0;i <360;i++){
    		theta = i*3.142/180;
    		glVertex2f(raio*cos(theta), raio*sin(theta));
    	}
    glEnd();
	// Executa os comandos OpenGL 
	glFlush();
	
}
 //Fun��o callback chamada quando o tamanho da janela � alterado 
void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
	GLsizei largura, altura;
                   
	// Evita a divisao por zero
	if(h == 0) h = 1;

	// Atualiza as vari�veis
	largura = w;
	altura = h;
                                              
	// Especifica as dimens�es da Viewport
	glViewport(0, 0, largura, altura);

	// Inicializa o sistema de coordenadas
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Estabelece a janela de sele��o (esquerda, direita, inferior, 
	// superior) mantendo a propor��o com a janela de visualiza��o
	if (largura <= altura) 
		gluOrtho2D (-40.0f, 40.0f, -40.0f*altura/largura, 40.0f*altura/largura);
	else 
		gluOrtho2D (-40.0f*largura/altura, 40.0f*largura/altura, -40.0f, 40.0f);
}

// Fun��o callback chamada para gerenciar eventos de teclas
void Teclado (unsigned char key, int x, int y)
{
	if (key == 27)
		bandeira();
}

// Fun��o respons�vel por inicializar par�metros e vari�veis
void Inicializa (void)
{   
	// Define a cor de fundo da janela de visualiza��o como branca
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
}

// Programa Principal 
int main(void)
{
	// Define do modo de opera��o da GLUT
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); 
 
	// Especifica a posi��o inicial da janela GLUT
	glutInitWindowPosition(5,5); 
    
	// Especifica o tamanho inicial em pixels da janela GLUT
	glutInitWindowSize(450,450); 
 
	// Cria a janela passando como argumento o t�tulo da mesma
	glutCreateWindow("Desenho de uma casa com rota��o");
 
	// Registra a fun��o callback de redesenho da janela de visualiza��o
	glutDisplayFunc(Desenha);
  
	// Registra a fun��o callback de redimensionamento da janela de visualiza��o
	glutReshapeFunc(AlteraTamanhoJanela);

	// Registra a fun��o callback para tratamento das teclas ASCII
	glutKeyboardFunc (Teclado);
    
	// Chama a fun��o respons�vel por fazer as inicializa��es 
	Inicializa();
 
	// Inicia o processamento e aguarda intera��es do usu�rio
	glutMainLoop();
 
	return 0;
}
