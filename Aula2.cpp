#include <GL/glut.h>
#include <stdlib.h>

void Desenhar(){
	// Limpa a janela de visualiza��o com a cor  
	// de fundo definida previamente
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Altera a cor do desenho para azul
	glColor3f(0.0f, 0.0f, 1.0f);
	
	//Desenha um quadrado = casa
	glBegin(GL_QUADS);
		glVertex2f(-15.0f, -15.0f);
		glVertex2f(-15.0f, 5.0f);
		glVertex2f(15.0f, 5.0f);
		glVertex2f(15.0f, -15.0f);
	glEnd();	
	
	//Altera a cor do desenho para branco
	glColor3f(1.0f, 1.0f, 1.0f);
	
	//Desenha um quadrado = porta
	glBegin(GL_QUADS);
		glVertex2f(-4.0f, -14.5f);
		glVertex2f(-4.0f, 0.0f);
		glVertex2f(4.0f, 0.0f);
		glVertex2f(4.0f, -14.5f);
	glEnd();
	
	//Desenha um quadrado = janela
	glBegin(GL_QUADS);
		glVertex2f(7.0f, -5.0f);
		glVertex2f(7.0f, -1.0f);
		glVertex2f(13.0f, -1.0f);
		glVertex2f(13.0f, -5.0f);
	glEnd();	
	//Desenha um quadrado = segunda janela
	glBegin(GL_QUADS);
		glVertex2f(-7.0f, -5.0f);
		glVertex2f(-7.0f, -1.0f);
		glVertex2f(-13.0f, -1.0f);
		glVertex2f(-13.0f, -5.0f);
	glEnd();
	
	//Altera a cor do desenho para azul
	glColor3f(0.0f, 0.0f, 1.0f);
	
	//Desenhar linhas das janelas
	glBegin(GL_LINES);
		glVertex2f(7.0f, -3.0f);
		glVertex2f(13.0f, -3.0f);
		glVertex2f(10.0f, -1.0f);
		glVertex2f(10.0f, -5.0f);
		
		//glVertex2f(-7.0f, -3.0f);
		//glVertex2f(-13.0f, -3.0f);
		//glVertex2f(-10.0f, -1.0f);
		//glVertex2f(-10.0f, -5.0f);
	glEnd();
	
	//Altera a cor do desenho para vermelho
	glColor3f(1.0f, 0.0f, 0.0f);
	/*
	//Desenhar tri�ngulo
	glBegin(GL_TRIANGLES);
		glVertex2f(-15.0f, 5.0f);
		glVertex2f(0.0f, 17.0f);
		glVertex2f(15.0f, 5.0f);
	glEnd();
	*/
	
	glColor3f(1.0f, 1.0f, 0.0f);
	
	//Desenhar tri�ngulo
	glBegin(GL_TRIANGLES);
		glVertex2f(-22.5f, 5.0f);
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex2f(-8.0f, 17.0f);
		glColor3f(0.0f, .0f, 1.0f);
		glVertex2f(0.0f, 5.0f);
	glEnd();
	
	glFlush();
}

void Inicializar(){
	//Define a cor de fundo da janela de visualiza��o como branca
	glClearColor(1,1,1,1);
}

// Fun��o callback chamada quando o tamanho da janela � alterado 
void Ajustar(GLsizei w, GLsizei h)
{
	GLsizei largura, altura;

	// Evita a divisao por zero
	if(h == 0) h = 1;

	// Atualiza as vari�veis
	largura = w;
	altura = h;

	// Especifica as dimens�es da Viewport
	glViewport(0, 0, largura, altura);

	// Inicializa o sistema de coordenadas
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Estabelece a janela de sele��o (esquerda, direita, inferior, 
	// superior) mantendo a propor��o com a janela de visualiza��o
	if (largura <= altura) 
		gluOrtho2D (-40.0f, 40.0f, -40.0f*altura/largura, 40.0f*altura/largura);
	else 
		gluOrtho2D (-40.0f*largura/altura, 40.0f*largura/altura, -40.0f, 40.0f);
}

int main(){
	//Define o modo de opera��o do GLUT
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	
	//Especifica a posi��o inicial da janela GLUT
	glutInitWindowPosition(5,5);
	
	//Especifica o tamanho inicial da janela GLUT em pixels
	glutInitWindowSize(450,450);
	
	//Criar janela com t�tulo
	glutCreateWindow("Casa");
	
	//Registra a fun��o callback de desenho da janela  de visualiza��o
	glutDisplayFunc(Desenhar);
	
	// Registra a fun��o callback de redimensionamento da janela de visualiza��o
	glutReshapeFunc(Ajustar);
	
	//Inicializa  alguns par�metros
	Inicializar();
	
	//Inicia o processamento e aguarda intera��es do usu�rio
	glutMainLoop();
	
	return 0;
}
