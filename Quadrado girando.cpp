#include <GL\glut.h>

float lado = 15;

void quadrado(){
	glColor3f(1,1,0);
	glBegin(GL_QUADS);
		glVertex2f(-lado, -lado);
		glVertex2f(lado, -lado);
		glVertex2f(lado, lado);
		glVertex2f(-lado, lado);
	glEnd();
}

void desenha(){
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT);
	quadrado();
	glRotatef(2,0,0,1);
	glutSwapBuffers();
}

void animar(int value){
	glutPostRedisplay();
	glutTimerFunc(30,animar,1);
}

void ajusta_tela(int w, int h){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-30,30,-30,30);
	glMatrixMode(GL_MODELVIEW);
}

int main(){
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowPosition(200,100);
	glutInitWindowSize(800,800);
	glutCreateWindow("QUADRADO GIRANDO");
	glutDisplayFunc(desenha);
	glutReshapeFunc(ajusta_tela);
	glutTimerFunc(30,animar,1);
	glutMainLoop();
}
