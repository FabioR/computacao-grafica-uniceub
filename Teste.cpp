#include <GL\glut.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

float rotationAngle;

void Desenhar(){
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	
	glColor3f(1,0,0);
	glBegin(GL_QUADS);
		glVertex2f(20,20);
		glVertex2f(-20,20);
		glVertex2f(-20,-20);
		glVertex2f(20,-20);
	glEnd();
	
	glRotatef(2,0,0,1);
	//glFlush();
	glutSwapBuffers();
}

void Keyboard(unsigned char key, int x, int y){
	//cout << (int)key<<endl;
}

void ajusta_tela(){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-50,50,-50,50);
	glMatrixMode(GL_MODELVIEW);
}

void anima(int timer){
	glutPostRedisplay();	
	glutTimerFunc(30,anima,1);
}
int main(){
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowPosition(200,100);
	glutInitWindowSize(800,600);
	glutCreateWindow("TESTE");
	glutDisplayFunc(Desenhar);
	glutDisplayFunc(ajusta_tela);
	glutKeyboardFunc(Keyboard);
	glutTimerFunc(30,anima,1);
	glutMainLoop();
	return 0;
}
